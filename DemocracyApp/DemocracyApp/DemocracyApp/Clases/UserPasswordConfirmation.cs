﻿namespace DemocracyApp.Clases
{
    public class UserPasswordConfirmation : User
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }

}
